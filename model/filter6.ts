/**
 * ibm-orchestrator-api
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { UsersFields } from './usersFields';


export interface Filter6 { 
    where?: object;
    fields?: UsersFields;
    offset?: number;
    limit?: number;
    skip?: number;
    order?: Array<string>;
}

